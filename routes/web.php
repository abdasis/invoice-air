<?php

use App\Http\Controllers\InvoiceController;
use App\Http\Livewire\Dashboard;
use App\Http\Livewire\Inventaris\Index as IndexInventari;
use App\Http\Livewire\Invoice\Index as InvoiceIndex;
use App\Http\Livewire\Pengguna\Create;
use App\Http\Livewire\Pengguna\Index;
use App\Http\Livewire\Pengguna\Update;
use App\Http\Livewire\Setting\Harga;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|

| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function (){
    Route::group(['prefix' => 'pengguna',], function (){
        Route::get('semua/{slug?}', Index::class)->name('pengguna.index');
        Route::get('tambah', Create::class)->name('pengguna.create');
        Route::get('sunting/{slug}', Update::class)->name('pengguna.update');
    });

    Route::group(['prefix'=>'invoice'], function (){
        Route::get('semua', InvoiceIndex::class)->name('invoice.index');
        Route::get('print/{id}', [InvoiceController::class, 'print'])->name('invoice.print');
        Route::get('export', [InvoiceController::class, 'export'])->name('invoice.export');
    });

    Route::group(['prefix' => 'pengaturan'], function (){
        Route::get('harga', Harga::class)->name('pengaturan.harga');
    });
    Route::group(['prefix'=> 'inventaris'], function (){
        Route::get('semua', IndexInventari::class)->name('inventari.index');
        Route::get('export', [\App\Http\Controllers\InventariController::class, 'export'])->name('inventari.export');
    });
    Route::get('transaksi', \App\Http\Livewire\Transaksi\Semua::class)->name('transaksi.semua');
    Route::get('keluar', function (){
        Auth::logout();
        return redirect()->route('login');
    })->name('keluar');
    Route::get('dark-mode', function (){
       return 'Hai';
    })->name('darkmode');
});

Route::get('register', function (){
    dd('Mandi Sana Bos');
})->name('register');
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', Dashboard::class)->name('dashboard');
