<?php


namespace App\Traits;


use Livewire\WithPagination;

trait Index
{
    use WithPagination;

    public  $pencarian,
        $paginate = 10,
        $sort = 'desc';

    public $warna = [
        'primary', 'danger', 'warning', 'info', 'blue', 'azure', 'purple', 'indigo',
        'pink', 'orange', 'teal',
        'primary-dim', 'danger-dim', 'warning-dim', 'info-dim', 'blue-dim', 'azure-dim', 'purple-dim', 'indigo-dim',
        'pink-dim', 'orange-dim', 'teal-dim',
    ];

    public function paginationView()
    {
        return 'vendor.livewire.bootstrap';
    }

    public function scopeSort($query,$asc, $desc)
    {
        if ($asc){
            return $query->orderBy('created_at', $asc);
        }elseif ($desc){
            return  $query->orderBy('created_at', $desc);
        }else{
            return $query->orderBy('created_at', 'desc');
        }
    }

    public function asc()
    {
        return $this->sort = 'asc';
    }

    public function desc()
    {
        return $this->sort = 'desc';
    }

    public function sepuluh()
    {
        $this->paginate = 10;
    }

    public function duaPuluh()
    {
        $this->paginate = 20;
    }

    public function limaPuluh()
    {
        $this->paginate = 50;
    }



}
