<?php

namespace App\Providers;

use App\Models\Harga;
use App\Models\Inventari;
use App\Models\Invoice;
use App\Observers\HargaObserver;
use App\Observers\InventariObserver;
use App\Observers\InvoiceObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Harga::observe(HargaObserver::class);
        Invoice::observe(InvoiceObserver::class);
        Inventari::observe(InventariObserver::class);
    }
}
