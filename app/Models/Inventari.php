<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inventari extends Model
{
    use HasFactory;

    public function scopePencarian($query, $kata)
    {
        return $query->orWhere('nama_barang', 'LIKE', '%' . $kata . '%')
            ->orWhere('harga_satuan', 'LIKE', '%' . $kata . '%')
            ->orWhere('harga_total', 'LIKE', '%' . $kata . '%');
    }
}
