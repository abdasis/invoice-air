<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pengguna extends Model
{
    use HasFactory;

    public function scopePencarian($query, $kata)
    {
        return $query->orWhere('nama_lengkap', 'LIKE', '%'.$kata.'%')
            ->orWhere('nomor_telepon', 'LIKE', '%'.$kata.'%')
            ->orWhere('nomor_meter', 'LIKE', '%'.$kata.'%')
            ->orWhere('alamat', 'LIKE', '%'.$kata.'%');
    }

    public function tagihan()
    {
        return $this->hasOne(Invoice::class);
    }

}
