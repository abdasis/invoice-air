<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Utils;

class Invoice extends Model
{
    use HasFactory;

    public function scopePencarian($query, $kata)
    {
        return $query->orWhere('pengguna_id', 'LIKE', '%' . $kata . '%')
            ->orWhere('created_at', 'LIKE', '%' . $kata . '%')
            ->orWhere('dibuat_oleh', 'LIKE', '%' . $kata . '%');
    }

    public function pengguna()
    {
        return $this->belongsTo(Pengguna::class);
    }
}
