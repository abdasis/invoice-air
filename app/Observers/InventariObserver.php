<?php

namespace App\Observers;

use App\Models\Inventari;
use Illuminate\Support\Facades\Auth;

class InventariObserver
{
    public function creating(Inventari $inventari)
    {
        $inventari->dibuat_oleh = Auth::user()->name;
        $inventari->diupdate_oleh = Auth::user()->name;
    }

    public function updating(Inventari $inventari)
    {
        $inventari->diupdate_oleh = Auth::user()->name;
    }
}
