<?php

namespace App\Observers;

use App\Models\Harga;
use App\Models\Pengguna;
use Illuminate\Support\Facades\Auth;

class HargaObserver
{
    public function creating(Harga $harga)
    {
        $harga->dibuat_oleh = Auth::user()->name;
        $harga->diperbarui_oleh  = Auth::user()->name;
    }

    public function updating(Harga $harga)
    {
        $harga->diperbarui_oleh = Auth::user()->name;
    }
}
