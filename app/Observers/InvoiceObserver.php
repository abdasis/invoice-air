<?php

namespace App\Observers;

use App\Models\Invoice;
use Illuminate\Support\Facades\Auth;

class InvoiceObserver
{
    public function creating(Invoice $invoice)
    {
        $invoice->dibuat_oleh = Auth::user()->name;
    }
}
