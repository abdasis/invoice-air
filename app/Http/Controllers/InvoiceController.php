<?php

namespace App\Http\Controllers;

use App\Exports\InvoiceExport;
use App\Models\Invoice;
use Illuminate\Http\Request;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;
use Maatwebsite\Excel\Facades\Excel;


class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $invoice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function edit(Invoice $invoice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invoice $invoice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invoice $invoice)
    {
        //
    }

    public function print($id)
    {
        $invoice = Invoice::find($id);
        $data = [
            'id' => $invoice->id,
            'tanggal' => $invoice->created_at,
            'nama_lengkap' => $invoice->pengguna->nama_lengkap,
            'nomor_meter' => $invoice->pengguna->nomor_meter,
            'alamat' => $invoice->pengguna->alamat,
            'meter_awal' => $invoice->awal_meter,
            'meter_akhir' => $invoice->akhir_meter,
            'pemakaian_kubik' => $invoice->pemakain_kubik,
            'biaya_perawatan' => $invoice->biaya_perawatan,
            'biaya_admin' => $invoice->biaya_admin,
            'diskon' => $invoice->diskon,
            'tagihan' => $invoice->total_tagihan,
            'harga_perkubik' => $invoice->harga_perkubik,
        ];

        $pdf = PDF::loadView('export.invoices', $data)
            ->setOption('margin-top', 0)
            ->setOption('margin-bottom', 0)
            ->setOption('margin-right', 0)
            ->setOption('margin-left', 0)
            ->setOption('page-width', '58mm')
            ->setOption('page-height', '110mm');
        return $pdf->inline();
    }

    public function export()
    {
        $fileName = 'laporan-penagihan-' . date('d-m-Y') . '.xlsx';
        return Excel::download(new InvoiceExport(), $fileName);
    }
}
