<?php

namespace App\Http\Livewire\Setting;

use App\Traits\DeleteConfirm;
use App\Traits\Index as IndexTrait;
use Livewire\Component;
use App\Models\Harga as ModelHarga;

class Harga extends Component
{
    use IndexTrait;

    public  $biaya_perawatan, $biaya_admin, $harga_perkubik, $harga_id;


    protected  $listeners = [
        'hapus',
        'batalkanHapus',
        'konfirmTambah',
        'batalTambah'
    ];

    public function rules()
    {
        return [
            'biaya_perawatan' => 'required|min:3',
            'biaya_admin' => 'required|min:3',
            'harga_perkubik' => 'required|min:3'
        ];
    }


    public function simpan()
    {
        $this->validate();
        $this->confirm('Yakin Simpan Harga Ini?', [
            'text' => 'Data harga akan diperbarui kedata ini!',
            'toast' => false,
            'position' => 'center',
            'confirmButtonText' => 'Ya',
            'showConfirmButton' => true,
            'cancelButtonText' => 'Tidak',
            'onConfirmed' => 'konfirmTambah',
            'onCancelled' => 'batalTambah'
        ]);

    }

    public function konfirmTambah()
    {
        $harga = new \App\Models\Harga();
        $harga->biaya_perawatan = $this->biaya_perawatan;
        $harga->biaya_admin = $this->biaya_admin;
        $harga->harga_perkubik = $this->harga_perkubik;
        $harga->save();
        if ($harga){
            $this->alert('success', 'Berhasil', [
                'text' => 'Data berhasil ditambahkan',
                'position' => 'center',
                'toast' => false
            ]);
        }
        $this->reset();
    }

    public function batalTambah()
    {
        $this->alert('info', 'Tidak jadi mengubah harga');
    }

    public function delete($id)
    {
        $this->harga_id = $id;
        $this->confirm('Yakin Hapus Data Ini?', [
            'text' => 'Data yang dihapus tidak dapat dikembalikan!',
            'toast' => false,
            'position' => 'center',
            'confirmButtonText' => 'Ya',
            'showConfirmButton' => true,
            'cancelButtonText' => 'Tidak',
            'onConfirmed' => 'hapus',
            'onCancelled' => 'batalkanHapus'
        ]);
    }

    public function batalkanHapus()
    {
        $this->alert('info', 'Harga tidak jadi dihapus');
    }

    public function hapus()
    {
        $harga = \App\Models\Harga::find($this->harga_id);
        if ($harga)
        {
            $harga->delete();
            $this->alert('error', 'Data berhasil dihapus');
        }
    }

    public function render()
    {
        return view('livewire.setting.harga', [
            'semuaHarga' => ModelHarga::latest()->paginate($this->paginate),
        ]);
    }
}
