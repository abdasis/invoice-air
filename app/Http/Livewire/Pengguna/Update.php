<?php

namespace App\Http\Livewire\Pengguna;

use App\Models\Pengguna;
use Illuminate\Support\Str;
use Livewire\Component;

class Update extends Component
{
    public  $nama_lengkap, $nomor_meter, $alamat, $nomor_telepon, $pengguna_id;

    public function rules()
    {
        return [
            'nama_lengkap' => 'required|min:3',
            'nomor_meter'=> 'required|min:3',
            'alamat' => 'required|min:3',
            'nomor_telepon' => 'required|min:5'
        ];
    }

    public function mount($slug)
    {
        $pengguna = Pengguna::whereSlug($slug)->first();
        $this->nama_lengkap = $pengguna->nama_lengkap;
        $this->nomor_meter = $pengguna->nomor_meter;
        $this->alamat    = $pengguna->alamat;
        $this->nomor_telepon = $pengguna->nomor_telepon;
        $this->pengguna_id = $pengguna->id;
    }
    public function update()
    {
        $this->validate();
        $pengguna = Pengguna::find($this->pengguna_id);
        $pengguna->nama_lengkap = Str::title($this->nama_lengkap);
        $pengguna->nomor_meter = $this->nomor_meter;
        $pengguna->alamat = Str::title($this->alamat);
        $pengguna->nomor_telepon = $this->nomor_telepon;
        $pengguna->slug = Str::slug($this->nama_lengkap);
        $pengguna->save();
        $this->alert('success', 'Berhasil',[
            'toast' => false,
            'text' => 'Data Berhasil Diperbarui',
            'position' => 'center'
        ]);

    }
    public function render()
    {
        return view('livewire.pengguna.update');
    }
}
