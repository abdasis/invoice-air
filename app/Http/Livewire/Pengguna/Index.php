<?php

namespace App\Http\Livewire\Pengguna;

use App\Models\Pengguna;
use App\Traits\Index as IndexTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use IndexTrait;
    protected $paginationTheme = 'bootstrap';

    protected $listeners = ['confirmed', 'cancelled'];


    public $pengguna_id;
    public function confirmed()
    {
        $pengguna = Pengguna::find($this->pengguna_id);
        $pengguna->delete();
        $this->alert('success', 'Data berhasil dihapus');
    }

    public function delete($id)
    {
        $this->confirm('Yakin Hapus Data Ini?', [
            'text' => 'Data yang dihapus tidak dapat dikembalikan!',
            'toast' => false,
            'position' => 'center',
            'showConfirmButton' => true,
            'cancelButtonText' => 'Tidak',
            'onConfirmed' => 'confirmed',
            'onCancelled' => 'cancelled'
        ]);
        $this->pengguna_id = $id;
    }
    public function render()
    {
        $semuaPengguna = Pengguna::query()->pencarian($this->pencarian)
            ->orderBy('created_at', $this->sort)
            ->paginate($this->paginate);
//        dd($semuaPengguna);
        return view('livewire.pengguna.index',[
            'semuaPengguna' => $semuaPengguna
        ]);
    }
}
