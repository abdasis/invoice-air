<?php

namespace App\Http\Livewire\Pengguna;

use App\Models\Pengguna;
use Illuminate\Support\Str;
use Livewire\Component;

class Create extends Component
{
    public  $nama_lengkap, $nomor_meter, $alamat, $nomor_telepon;

    public function rules()
    {
        return [
            'nama_lengkap' => 'required|min:3',
            'nomor_meter'=> 'required|min:3',
            'alamat' => 'required|min:3',
            'nomor_telepon' => 'required|min:5'
        ];
    }
    public function simpan()
    {
        $this->validate();
        $pengguna = new Pengguna();
        $pengguna->nama_lengkap = Str::title($this->nama_lengkap);
        $pengguna->nomor_meter = $this->nomor_meter;
        $pengguna->alamat = Str::title($this->alamat);
        $pengguna->nomor_telepon = $this->nomor_telepon;
        $pengguna->slug = Str::slug($this->nama_lengkap);
        $pengguna->save();
        $this->alert('success', 'Berhasil',[
            'toast' => false,
            'text' => 'Data Berhasil Disimpan',
            'position' => 'center'
        ]);
        $this->reset();

    }
    public function render()
    {
        return view('livewire.pengguna.create');
    }
}
