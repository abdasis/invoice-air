<?php

namespace App\Http\Livewire\Transaksi;

use App\Models\Invoice;
use App\Traits\Index;
use Livewire\Component;

class Semua extends Component
{
    use Index;
    public function render()
    {

        if ($this->pencarian){
            $transaksi = Invoice::query()
                ->leftJoin('penggunas', 'invoices.pengguna_id', 'penggunas.id' )
                ->orWhere('penggunas.nama_lengkap', $this->pencarian)
                ->orderBy('invoices.created_at', 'asc')
                ->paginate($this->paginate);
        }else{
            $transaksi = Invoice::query()
                ->leftJoin('penggunas', 'invoices.pengguna_id', 'penggunas.id' )
                ->orderBy('invoices.created_at', 'asc')
                ->paginate($this->paginate);
        }
        return view('livewire.transaksi.semua', [
            'semua_transaksi' => $transaksi
        ]);
    }
}
