<?php

namespace App\Http\Livewire\Invoice;

use App\Exports\InvoiceExport;
use App\Models\Invoice;
use App\Models\Pengguna;
use Livewire\Component;
use App\Traits\Index as IndexPage;
use Maatwebsite\Excel\Facades\Excel;

class Index extends Component
{
    use IndexPage;

    public $statusUpdate = false;
    public $invoice_id;

    protected $listeners = [
        'confirmed',
        'cancelled',
        'hapusInvoice',
        'tidakJadi',
        'invoiceDiUpdate' => 'updateInvoice',
        'invoiceDitambah' => 'updateInvoice'
    ];

    public function updateInvoice($invoice)
    {
        $this->alert('success', 'Tagihan Berhasil Diupdate');
        $this->emit('closeModal');

    }

    public function invoiceDitambah($invoice)
    {
        $this->alert('success', 'Tagihan Berhasil Dibuat');
        $this->emit('closeModal');

    }


    public function tambahInvoice()
    {
        $this->statusUpdate = false;
    }

    public function suntingInvoice($id)
    {
        $this->statusUpdate = true;
        $invoice = Invoice::find($id);
        $this->emit('getInvoice', $invoice);
    }

    public function terima($id)
    {
        $this->confirm('Yakin sudah melakukan pembayaran?', [
            'toast' => false,
            'position' => 'center',
            'showConfirmButton' => true,
            'cancelButtonText' => 'Tidak',
            'onConfirmed' => 'confirmed',
            'onCancelled' => 'cancelled'
        ]);

        $this->invoice_id = $id;

    }

    public function confirmed()
    {
        $invoice = Invoice::find($this->invoice_id);
        if ($invoice->status == 'lunas')
        {
            $this->alert('warning', 'Penagihan ini sudah dibayar');
        }else{
            $invoice->status = 'lunas';
            $invoice->save();
            $this->alert('success', 'Tagihan Berhasil Dibayar');
        }
    }

    public function cancelled()
    {
        $this->alert('info', 'Gagal Diterima');
    }

    public function delete($id)
    {
        $this->confirm('Yakin hapus data ini?', [
            'text'=> 'Data yang dihapus tidak dapat dikembalikan',
            'toast' => false,
            'position' => 'center',
            'showConfirmButton' => true,
            'cancelButtonText' => 'Tidak',
            'onConfirmed' => 'hapusInvoice',
            'onCancelled' => 'tidakJadi'
        ]);
        $this->invoice_id = $id;
    }

    public function hapusInvoice()
    {
        $invoice = Invoice::find($this->invoice_id);
        $invoice->delete();
        $this->alert('success', 'Data berhasil dihapus');
    }

    public function tidakJadi()
    {
        $this->alert('info', 'Invoice tidak jadi dihapus');
    }

    public function export()
    {
        return Excel::download(new InvoiceExport, 'users.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }
    public function render()
    {
        if ($this->pencarian)
        {
            $semuaInvoice = Invoice::join('penggunas', 'invoices.pengguna_id', 'penggunas.id')
                ->where('status', 'belum bayar')
                ->where('penggunas.nama_lengkap', $this->pencarian)
                ->orderBy('invoices.created_at', $this->sort)
                ->select('penggunas.*','invoices.*', 'invoices.id as id')
                ->paginate($this->paginate);

        }else{
            $semuaInvoice = Invoice::join('penggunas', 'invoices.pengguna_id', 'penggunas.id')
                ->where('status', 'belum bayar')
                ->orderBy('invoices.created_at', $this->sort)
                ->select('penggunas.*','invoices.*', 'invoices.id as id')
                ->paginate($this->paginate);
        }
        return view('livewire.invoice.index', [
            'semuaInvoice' => $semuaInvoice,
            'warna' => $this->warna,
        ]);
    }
}
