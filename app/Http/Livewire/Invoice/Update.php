<?php

namespace App\Http\Livewire\Invoice;

use App\Models\Harga;
use App\Models\Invoice;
use App\Models\Pengguna;
use Livewire\Component;

class Update extends Component
{
    public  $pengguna, $akhir_meter, $diskon, $invoice_id;

    protected $listeners = [
        'getInvoice' => 'showInvoice'
    ];

    public function showInvoice($invoice)
    {
        $this->pengguna = $invoice['pengguna_id'];
        $this->akhir_meter = $invoice['akhir_meter'];
        $this->diskon = $invoice['diskon'];
        $this->invoice_id = $invoice['id'];
    }

    public function update()
    {
        /*mendapatkan data pengguna*/
        $pengguna = Pengguna::find($this->pengguna);

        /*mengambil harga dari pengaturan harga terbaru*/
        $harga = Harga::latest()->first();

        /*mendapatkan awal meter*/
        $invoiceTerakhir = Invoice::wherePenggunaId($pengguna->id)->latest()->first();

        /*membuat kalkulasi otomatis pada meter awal*/
        $meterAwal = 0;
        if (empty($invoiceTerakhir)) {
            $meterAwal = 0;
        } else {
            $meterAwal = $invoiceTerakhir->akhir_meter;
        }




        /*membuat fungsi untuk menyimpan data invoice*/
        $invoice = Invoice::find($this->invoice_id);

        $invoice->diskon = $this->diskon;
        $invoice->total_tagihan = ($invoice->total_tagihan - $this->diskon);
        $invoice->status = 'belum bayar';
        $pengguna->tagihan()->save($invoice);
        if ($pengguna)
        {
            $this->emit('invoiceDiUpdate', $invoice);

        }
    }

    public function render()
    {
        return view('livewire.invoice.update',[
            'semuaPengguna' => Pengguna::latest()->get(),
        ]);
    }
}
