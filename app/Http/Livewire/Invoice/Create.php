<?php

namespace App\Http\Livewire\Invoice;

use App\Models\Harga;
use App\Models\Invoice;
use App\Models\Pengguna;
use Livewire\Component;

class Create extends Component
{
    public  $pengguna, $akhir_meter, $diskon;

    public function simpan()
    {
        /*mendapatkan data pengguna*/
        $pengguna = Pengguna::find($this->pengguna);

        /*mengambil harga dari pengaturan harga terbaru*/
        $harga = Harga::latest()->first();

        /*mendapatkan awal meter*/
        $invoiceTerakhir = Invoice::wherePenggunaId($pengguna->id)->whereMonth('created_at', date('m'))->latest()->first();

        /*membuat kalkulasi otomatis pada meter awal*/
        $meterAwal = 0;
        if (empty($invoiceTerakhir)) {
            $meterAwal = 0;
        } else {
            $meterAwal = $invoiceTerakhir->akhir_meter;
        }

         /*cek tagihan bulan ini*/
         $invoiceBulanIni = Invoice::wherePenggunaId($pengguna->id)->whereMonth('created_at', date('m'))->first();

         if ($invoiceBulanIni){
             return $this->alert('warning', 'Maaf!',[
                 'text' => 'Maaf pengguna ini sudah melakukan pembayaran bulan ini!',
                 'toast' => false,
                 'position' => 'center'
             ]);
         }


        /*membuat fungsi untuk menyimpan data invoice*/
        $invoice = new Invoice();
        $invoice->awal_meter = $meterAwal;
        $invoice->akhir_meter = $this->akhir_meter;
        $invoice->pemakaian_liter = $this->akhir_meter - $meterAwal;
        $invoice->pemakain_kubik = ($this->akhir_meter - $meterAwal) / 1000;
        $invoice->biaya_admin = $harga->biaya_admin;
        $invoice->biaya_admin = $harga->biaya_admin;
        $invoice->biaya_perawatan = $harga->biaya_perawatan;
        $invoice->harga_perkubik = $harga->harga_perkubik;
        $invoice->diskon = $this->diskon;
        $invoice->total_tagihan = ($invoice->pemakain_kubik * $harga->harga_perkubik) +
            $harga->biaya_admin +
            $harga->biaya_perawatan -
            $this->diskon;
        $invoice->status = 'belum bayar';
        $pengguna->tagihan()->save($invoice);
        if ($pengguna)
        {
            $this->emit('invoiceDitambah', $invoice);
            $this->reset();
        }
    }

    public function render()
    {
        return view('livewire.invoice.create',[
            'semuaPengguna' => Pengguna::latest()->get(),

        ]);
    }
}
