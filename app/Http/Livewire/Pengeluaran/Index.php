<?php

namespace App\Http\Livewire\Pengeluaran;

use Livewire\Component;

class Index extends Component
{
    public function render()
    {
        return view('livewire.pengeluaran.index');
    }
}
