<?php

namespace App\Http\Livewire\Pengeluaran;

use Livewire\Component;

class Create extends Component
{
    public function render()
    {
        return view('livewire.pengeluaran.create');
    }
}
