<?php

namespace App\Http\Livewire\Inventaris;

use App\Models\Inventari;
use Livewire\Component;

class Index extends Component
{
    use \App\Traits\Index;

    public  $statusUpdate = false;
    public  $inventari_id;
    protected $listeners = [
        'update' => 'diupdate',
        'hapusInventari',
        'batalHapus'
    ];

    public function diupdate()
    {
        $this->alert('success', 'Data berhasil diperbarui');
    }

    public function tambahInventari()
    {
        $this->statusUpdate = false;
        $this->emit('tambahInventari');
    }

    public function editInventari($id)
    {
        $this->statusUpdate = true;
        $inventari = Inventari::find($id);
        $this->emit('editInventari', $inventari);
    }

    public function delete($id)
    {
        $this->inventari_id = $id;
        $this->confirm('Yakin hapus data ini?', [
            'toast' => false,
            'text' => 'Data tidak dapat dikembalikan lagi!',
            'position' => 'center',
            'showConfirmButton' => true,
            'cancelButtonText' => 'Tidak',
            'onConfirmed' => 'hapusInventari',
            'onCancelled' => 'batalHapus'
        ]);

    }

    public function hapusInventari()
    {
        $inventari = Inventari::find($this->inventari_id);
        if ($inventari)
        {
            $inventari->delete();
            $this->alert('success', 'Data berhasil dihapus');
        }
    }

    public function batalHapus()
    {
        return $this->alert('info', 'Tidak jadi menghapus');
    }

    public function render()
    {
        $semuaInventari = Inventari::query()->pencarian($this->pencarian)
            ->orderBy('created_at', $this->sort)
            ->paginate($this->paginate);
        return view('livewire.inventaris.index',[
            'semuaInventari' => $semuaInventari
        ]);
    }
}
