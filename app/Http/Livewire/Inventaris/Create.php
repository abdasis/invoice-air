<?php

namespace App\Http\Livewire\Inventaris;

use App\Models\Inventari;
use Livewire\Component;

class Create extends Component
{
    public  $nama_barang, $jumlah, $harga_satuan, $satuan, $tanggal_pengeluaran;

    public function rules()
    {
        return [
            'nama_barang' => 'required|min:5',
            'jumlah' => 'required',
            'harga_satuan' => 'required'
        ];
    }

    public function simpan()
    {
        $this->validate();
        $inventari = new Inventari();
        $inventari->nama_barang = $this->nama_barang;
        $inventari->jumlah = $this->jumlah;
        $inventari->satuan = $this->satuan;
        $inventari->tanggal_pengeluaran = $this->tanggal_pengeluaran;
        $inventari->harga_satuan = $this->harga_satuan;
        $inventari->harga_total = ($this->jumlah*$this->harga_satuan);
        $inventari->save();
        if ($inventari)
        {
            $this->alert('success', 'Data berhasil disimpan');
            $this->reset();
        }
    }
    public function render()
    {
        return view('livewire.inventaris.create');
    }
}
