<?php

namespace App\Http\Livewire\Inventaris;

use Livewire\Component;

class Show extends Component
{
    public function render()
    {
        return view('livewire.inventaris.show');
    }
}
