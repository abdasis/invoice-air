<?php

namespace App\Http\Livewire\Inventaris;

use App\Models\Inventari;
use Livewire\Component;

class Update extends Component
{
    public  $nama_barang, $jumlah, $harga_satuan, $satuan, $tanggal_pengeluaran, $inventari_id;

    public  $listeners = [
        'editInventari' => 'showInventari'
    ];

    public function showInventari($inventari)
    {
        $this->nama_barang = $inventari['nama_barang'];
        $this->jumlah = $inventari['jumlah'];
        $this->harga_satuan = $inventari['harga_satuan'];
        $this->satuan = $inventari['satuan'];
        $this->inventari_id = $inventari['id'];
    }

    public function update()
    {
        $inventari = Inventari::find($this->inventari_id);
        $inventari->nama_barang = $this->nama_barang;
        $inventari->jumlah = $this->jumlah;
        $inventari->tanggal_pengeluaran = $this->tanggal_pengeluaran;
        $inventari->harga_satuan = $this->harga_satuan;
        $inventari->satuan = $this->satuan;
        $inventari->save();

        if ($inventari)
        {
            $this->emit('update');
        }

    }
    public function render()
    {
        return view('livewire.inventaris.update');
    }
}
