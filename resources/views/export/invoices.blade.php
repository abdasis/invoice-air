<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        body{
            width: 58mm !important;
            height: 110mm !important;
        }
        p{
            font-size: 12px;
        }
        td{
            font-size: 12px;
            line-height: 13px;
        }
        table{
            width: 100%;
        }
        *{
            margin: 0px;
            padding: 2px;
        }
        .border{
            border: 1px dashed #444;
        }
        .text-center{
            text-align: center;
        }

        p.text-center{
            font-size: 12px;
        }

        .text-right{
            text-align: right !important;
        }
    </style>
</head>

<body>
<div class="lebar-cetak">
    <div class="row justify-content-center">
        <div class="col-md-4 shadow-sm">
            <h6 class="text-center">PMB AIR BERSIH</h6>
            <p class="text-center">JL. BATU KENNONG DS. GEGER TELP. 0877-7776-7778 / 0822-3106-8160</p>
            <p class="text-center">Tagihan : {{ date('d-F-Y') }}</p>
        </div>
    </div>
    <div class="border"></div>

    <table>
        <tr>
            <td>ID</td>
            <td>:</td>
            <td class="text-right"> {{ $id }}</td>
        </tr>
        <tr>
            <td>Nomor Refrensi</td>
            <td>:</td>
            <td class="text-right">{{ date('dmy') . $id }}</td>
        </tr>

        <tr>
            <td>Nama</td>
            <td>:</td>
            <td class="text-right">{{ $nama_lengkap }}</td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td>:</td>
            <td class="text-right">{{ $alamat }}</td>
        </tr>
        <tr>
            <td>No Meter</td>
            <td>:</td>
            <td class="text-right">{{ $nomor_meter }}</td>
        </tr>
        <tr>
            <td>Meter Awal</td>
            <td>:</td>
            <td class="text-right">{{ $meter_awal }}</td>
        </tr>
        <tr>
            <td>Meter Akhir</td>
            <td>:</td>
            <td class="text-right">{{ $meter_akhir }}</td>
        </tr>
        <tr>
            <td>Jumlah Pekaian</td>
            <td>:</td>
            <td class="text-right">{{ $pemakaian_kubik }}m<sup>3</sup></td>
        </tr>
        <tr>
            <td>Biaya Perawatan</td>
            <td>:</td>
            <td class="text-right">Rp. {{ rupiah($biaya_perawatan) }}</td>
        </tr>
        <tr>
            <td>Harga Perkubik</td>
            <td>:</td>
            <td class="text-right">Rp. {{ rupiah($harga_perkubik) }}</td>
        </tr>
        <tr>
            <td>Diskon</td>
            <td>:</td>
            <td class="text-right">Rp. {{ rupiah($diskon) }}</td>
        </tr>
        <tr>
            <td>Total</td>
            <td>:</td>
            <td class="text-right">Rp. {{ rupiah($tagihan) }}</td>
        </tr>
        <tr>
            <td>Terbilang</td>
            <td>:</td>
            <td class="text-right">{{Str::title(terbilang($tagihan))}}</td>
        </tr>
    </table>
    <div class="border"></div>
</div>
</body>

</html>
