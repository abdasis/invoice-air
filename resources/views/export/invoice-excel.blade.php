<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<table>
    <tr>
        <th>Nama Lengkap</th>
        <th>Nomor Meter</th>
        <th>Awal Meter</th>
        <th>Akhir Meter</th>
        <th>Pemakaian Liter</th>
        <th>Pemakaian Kubik</th>
        <th>Diskon</th>
        <th>Total Tagihan</th>
    </tr>
    @foreach($semuaInvoice as $invoice)
        <tr>
            <td>{{$invoice->pengguna->nama_lengkap}}</td>
            <td>{{$invoice->pengguna->nomor_meter}}</td>
            <td>{{$invoice->awal_meter}}</td>
            <td>{{$invoice->akhir_meter}}</td>
            <td>{{$invoice->pemakaian_liter}} l</td>
            <td>{{$invoice->pemakain_kubik}} m<sup>3</sup> </td>
            <td>Rp. {{rupiah($invoice->diskon)}}</td>
            <td>Rp. {{rupiah($invoice->total_tagihan)}}</td>
        </tr>
    @endforeach
</table>
</body>
</html>
