<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<table>
    <tr>
        <td>No.</td>
        <td>Nama Barang</td>
        <td>Jumlah</td>
        <td>Harga Satuan</td>
        <td>Total Harga</td>
        <td>Tanggal Pegeluaran</td>
    </tr>
    @foreach($semuaPengeluaran as $key => $pengeluaran)
        <tr>
            <td>{{$key+1}}</td>
            <td>{{$pengeluaran->nama_barang}}</td>
            <td>{{$pengeluaran->jumlah}} {{$pengeluaran->satuan}}</td>
            <td>Rp .{{rupiah($pengeluaran->harga_satuan)}}</td>
            <td>Rp. {{rupiah($pengeluaran->harga_total)}}</td>
            <td>{{$pengeluaran->tanggal_pengeluaran}}</td>
        </tr>
    @endforeach
</table>
</body>
</html>
