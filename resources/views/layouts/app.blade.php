<!DOCTYPE html>
<html lang="zxx" class="js">

<head>
    <base href="{{url('/')}}">
    <meta charset="utf-8">
    <meta name="author" content="Abd. Asis">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Starter Template Untuk Laravel Project dengan Dashlite Admin Theme">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <!-- Fav Icon  -->

    <link rel="shortcut icon" href="{{asset('images/favicon.png')}}">
    <!-- Page Title  -->
    <title>Harva Invoice | PMB Geger</title>
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="{{asset('assets/css/dashlite.css?ver=2.2.0')}}">
    <link id="skin-default" rel="stylesheet" href="{{asset('assets/css/theme.css?ver=2.2.0')}}">
    @livewireStyle
    @stack('css')
</head>

<body class="nk-body bg-white has-sidebar ">
<div class="nk-app-root">
    <!-- main @s -->
    <div class="nk-main ">
        <!-- sidebar @s -->
        <div class="nk-sidebar nk-sidebar-fixed " data-content="sidebarMenu">
            @component('components.sidebar')
            @endcomponent
        </div>
        <!-- sidebar @e -->
        <!-- wrap @s -->
        <div class="nk-wrap ">
            <!-- main header @s -->
            @component('components.header')
            @endcomponent
            <!-- main header @e -->
            <!-- content @s -->
            <div class="nk-content nk-content-fluid">
                <div class="container-xl wide-lg">
                    <div class="nk-content-body">
                        {{$slot}}
                    </div>
                </div>
            </div>

            @stack('modal')
            <!-- content @e -->
            <!-- footer @s -->
            @component('components.footer')
            @endcomponent
            <!-- footer @e -->
        </div>
        <!-- wrap @e -->
    </div>
    <!-- main @e -->
</div>
<!-- app-root @e -->
<!-- JavaScript -->
<script src="{{asset('assets/js/bundle.js?ver=2.2.0')}}"></script>
<script src="{{asset('assets/js/scripts.js?ver=2.2.0')}}"></script>
<script src="{{asset('assets/js/charts/gd-default.js?ver=2.2.0')}}"></script>
@livewireScripts
<x-livewire-alert::scripts />
@stack('js')

</body>

</html>
