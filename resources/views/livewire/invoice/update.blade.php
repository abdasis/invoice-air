<div>
    {{-- If you look to others for fulfillment, you will never truly be fulfilled. --}}
    <form wire:submit.prevent="update">
        <div class="form-group">
            <label class="form-label" for="full-name">Pilih Pengguna</label>
            <div class="form-control-wrap"wire:ignore>
                <div class="form-icon form-icon-left">
                    <em class="icon ni ni-user-alt"></em>
                </div>
                <select wire:model="pengguna" disabled id="form-pengguna" class="custom-select form-control form-control-lg">
                    @foreach($semuaPengguna as $pengguna)
                        <option value="{{$pengguna->id}}">{{$pengguna->nama_lengkap}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="form-label" for="full-name">Angka Meter Sekarang</label>
            <div class="form-control-wrap">
                <div class="form-icon form-icon-left">
                    <em class="icon ni ni-speed"></em>
                </div>
                <input type="text" wire:model="akhir_meter" class="form-control" id="akhir_meter" placeholder="Masukan Meter Akhir">
            </div>
        </div>
        <div class="form-group">
            <label class="form-label" for="full-name">Diskon</label>
            <div class="form-control-wrap"><div class="form-icon form-icon-left">
                    <em class="icon ni ni-percent"></em>
                </div>
                <input wire:model="diskon" type="text" class="form-control" id="diskon" placeholder="1000">
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-dark">Perbarui Invoice</button>
        </div>
    </form>
</div>
