<div>
    {{-- In work, do what you enjoy. --}}
    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">Daftar Invoice</h3>
                <div class="nk-block-des text-soft">
                    <p>Terdapat {{$semuaInvoice->total()}} pada saat ini</p>
                </div>
            </div><!-- .nk-block-head-content -->
            <div class="nk-block-head-content">
                <div class="toggle-wrap nk-block-tools-toggle">
                    <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="pageMenu"><em class="icon ni ni-menu-alt-r"></em></a>
                    <div class="toggle-expand-content" data-content="pageMenu">
                        <ul class="nk-block-tools g-3">
                            <li><a href="{{route('invoice.export')}}" class="btn btn-white btn-outline-light"><em class="icon ni ni-download-cloud"></em><span>Export</span></a></li>
                            <li class="nk-block-tools-opt">
                                <div class="drodown">
                                    <a wire:click.prevent="$emit('tambahInvoice')" href="{{route('pengguna.create')}}" class="dropdown-toggle btn btn-icon btn-primary"><em class="icon ni ni-plus"></em></a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div><!-- .toggle-wrap -->
            </div><!-- .nk-block-head-content -->
        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block">
        <div class="card card-bordered card-stretch">
            <div class="card-inner-group">
                <div class="card-inner position-relative card-tools-toggle">
                    <div class="card-title-group">
                        <div class="card-tools">
                            <div class="form-inline flex-nowrap gx-3">
                                <h5>Semua Data Invoice</h5>
                            </div><!-- .form-inline -->
                        </div><!-- .card-tools -->
                        <div class="card-tools mr-n1">
                            <ul class="btn-toolbar gx-1">
                                <li>
                                    <a href="#" class="btn btn-icon search-toggle toggle-search" data-target="search"><em class="icon ni ni-search"></em></a>
                                </li><!-- li -->
                                <li class="btn-toolbar-sep"></li><!-- li -->
                                <li>
                                    <div class="toggle-wrap">
                                        <a href="#" class="btn btn-icon btn-trigger toggle" data-target="cardTools"><em class="icon ni ni-menu-right"></em></a>
                                        <div class="toggle-content" data-content="cardTools">
                                            <ul class="btn-toolbar gx-1">
                                                <li class="toggle-close">
                                                    <a href="#" class="btn btn-icon btn-trigger toggle" data-target="cardTools"><em class="icon ni ni-arrow-left"></em></a>
                                                </li><!-- li -->
                                                <li>
                                                    <div class="dropdown">
                                                        <a href="#" class="btn btn-trigger btn-icon dropdown-toggle" data-toggle="dropdown">
                                                            <div class="dot dot-primary"></div>
                                                            <em class="icon ni ni-filter-alt"></em>
                                                        </a>
                                                        <div class="filter-wg dropdown-menu dropdown-menu-xl dropdown-menu-right">
                                                            <div class="dropdown-head">
                                                                <span class="sub-title dropdown-title">Filter Users</span>
                                                                <div class="dropdown">
                                                                    <a href="#" class="btn btn-sm btn-icon">
                                                                        <em class="icon ni ni-more-h"></em>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="dropdown-body dropdown-body-rg">
                                                                <div class="row gx-6 gy-3">
                                                                    <div class="col-6">
                                                                        <div class="custom-control custom-control-sm custom-checkbox">
                                                                            <input type="checkbox" class="custom-control-input" id="hasBalance">
                                                                            <label class="custom-control-label" for="hasBalance"> Have Balance</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <div class="custom-control custom-control-sm custom-checkbox">
                                                                            <input type="checkbox" class="custom-control-input" id="hasKYC">
                                                                            <label class="custom-control-label" for="hasKYC"> KYC Verified</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <div class="form-group">
                                                                            <label class="overline-title overline-title-alt">Role</label>
                                                                            <select class="form-select form-select-sm">
                                                                                <option value="any">Any Role</option>
                                                                                <option value="investor">Investor</option>
                                                                                <option value="seller">Seller</option>
                                                                                <option value="buyer">Buyer</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <div class="form-group">
                                                                            <label class="overline-title overline-title-alt">Status</label>
                                                                            <select class="form-select form-select-sm">
                                                                                <option value="any">Any Status</option>
                                                                                <option value="active">Active</option>
                                                                                <option value="pending">Pending</option>
                                                                                <option value="suspend">Suspend</option>
                                                                                <option value="deleted">Deleted</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-12">
                                                                        <div class="form-group">
                                                                            <button type="button" class="btn btn-secondary">Filter</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="dropdown-foot between">
                                                                <a class="clickable" href="#">Reset Filter</a>
                                                                <a href="#">Save Filter</a>
                                                            </div>
                                                        </div><!-- .filter-wg -->
                                                    </div><!-- .dropdown -->
                                                </li><!-- li -->
                                                <li>
                                                    <div class="dropdown">
                                                        <a href="#" class="btn btn-trigger btn-icon dropdown-toggle" data-toggle="dropdown">
                                                            <em class="icon ni ni-setting"></em>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-xs dropdown-menu-right">
                                                            <ul class="link-check">
                                                                <li><span>Show</span></li>
                                                                <li class="{{$paginate == 10 ? 'active' : ''}}"><a wire:click="sepuluh">10</a></li>
                                                                <li class="{{$paginate == 20 ? 'active' : ''}}"><a wire:click="duaPuluh">20</a></li>
                                                                <li class="{{$paginate == 50 ? 'active' : ''}}"><a wire:click="limaPuluh">50</a></li>
                                                            </ul>
                                                            <ul class="link-check">
                                                                <li><span>Order</span></li>
                                                                <li class="{{$sort == 'desc' ? 'active' : ''}}"><a wire:click="desc">DESC</a></li>
                                                                <li class="{{$sort == 'asc' ? 'active' : ''}}"><a wire:click="asc">ASC</a></li>
                                                            </ul>
                                                        </div>
                                                    </div><!-- .dropdown -->
                                                </li><!-- li -->
                                            </ul><!-- .btn-toolbar -->
                                        </div><!-- .toggle-content -->
                                    </div><!-- .toggle-wrap -->
                                </li><!-- li -->
                            </ul><!-- .btn-toolbar -->
                        </div><!-- .card-tools -->
                    </div><!-- .card-title-group -->
                    <div class="card-search search-wrap" data-search="search" wire:ignore>
                        <div class="card-body">
                            <div class="search-content">
                                <a href="#" class="search-back btn btn-icon toggle-search" data-target="search"><em class="icon ni ni-arrow-left"></em></a>
                                <input wire:model="pencarian" type="text" class="form-control border-transparent form-focus-none" placeholder="Search by user or email">
                                <button class="search-submit btn btn-icon"><em class="icon ni ni-search"></em></button>
                            </div>
                        </div>
                    </div><!-- .card-search -->
                </div><!-- .card-inner -->
                <div class="card-inner p-0">
                    <div class="nk-tb-list nk-tb-ulist">
                        <div class="nk-tb-item nk-tb-head">
                            <div class="nk-tb-col"><span class="sub-text">Nama Lengkap</span></div>
                            <div class="nk-tb-col tb-col-mb"><span class="sub-text">Pemakaian</span></div>
                            <div class="nk-tb-col tb-col-md"><span class="sub-text">Pemakain Kubik</span></div>
                            <div class="nk-tb-col tb-col-lg"><span class="sub-text">Diskon</span></div>
                            <div class="nk-tb-col tb-col-lg"><span class="sub-text">Total Tagihan</span></div>
                            <div class="nk-tb-col tb-col-lg"><span class="sub-text">Tanggal Dibuat</span></div>
                            <div class="nk-tb-col tb-col-md"><span class="sub-text">Status</span></div>
                            <div class="nk-tb-col nk-tb-col-tools text-right">
                                <div class="dropdown">
                                    <a href="#" class="btn btn-xs btn-outline-light btn-icon dropdown-toggle" data-toggle="dropdown" data-offset="0,5"><em class="icon ni ni-plus"></em></a>
                                    <div class="dropdown-menu dropdown-menu-xs dropdown-menu-right">
                                        <ul class="link-tidy sm no-bdr">
                                            <li>
                                                <div class="custom-control custom-control-sm custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" checked="" id="bl">
                                                    <label class="custom-control-label" for="bl">Balance</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="custom-control custom-control-sm custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" checked="" id="ph">
                                                    <label class="custom-control-label" for="ph">Phone</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="custom-control custom-control-sm custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="vri">
                                                    <label class="custom-control-label" for="vri">Verified</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="custom-control custom-control-sm custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="st">
                                                    <label class="custom-control-label" for="st">Status</label>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div><!-- .nk-tb-item -->
                        @foreach($semuaInvoice as $key => $invoice)
                            <div class="nk-tb-item">
                                <div class="nk-tb-col">
                                    <a>
                                        <div class="user-card">
                                            <div class="user-avatar bg-{{$warna[array_rand($warna)]}}">
                                                <span>{{$invoice->pengguna->nama_lengkap[0]}}</span>
                                            </div>
                                            <div class="user-info">
                                                <span class="tb-lead">{{$invoice->pengguna->nama_lengkap}} <span class="dot dot-success d-md-none ml-1"></span></span>
                                                <span>{{$invoice->pengguna->nomor_meter}}</span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="nk-tb-col tb-col-mb">
                                    <span class="tb-amount">{{$invoice->pemakaian_liter}}l<span class="currency"></span></span>
                                </div>
                                <div class="nk-tb-col tb-col-md">
                                    <span>{{$invoice->pemakain_kubik}}m<sup>3</sup></span>
                                </div>
                                <div class="nk-tb-col tb-col-md">
                                    <span>Rp. {{rupiah($invoice->diskon)}}</span>
                                </div> <div class="nk-tb-col tb-col-md">
                                    <span>Rp. {{rupiah($invoice->total_tagihan)}}</span>
                                </div>
                                <div class="nk-tb-col tb-col-lg">
                                    <span>{{\Carbon\Carbon::parse($invoice->created_at)->format('d F Y')}}</span>
                                </div>
                                <div class="nk-tb-col tb-col-md">
                                    <span class="badge badge-dot {{$invoice->status == 'lunas' ? 'text-success' : 'text-danger'}}">{{Str::title($invoice->status)}}</span>
                                </div>
                                <div class="nk-tb-col nk-tb-col-tools">
                                    <ul class="nk-tb-actions gx-1">
                                        <li class="nk-tb-action-hidden">
                                            <a wire:click.prevent="suntingInvoice({{$invoice->id}})" href="{{route('pengguna.update', $invoice->id)}}" class="btn btn-trigger btn-icon" data-toggle="tooltip" data-placement="top" title="Sunting">
                                                <em class="icon ni ni-pen-alt-fill"></em>
                                            </a>
                                        </li>
                                        <li class="nk-tb-action-hidden">
                                            <a href="#" wire:click.prevent="delete({{$invoice->id}})" class="btn text-danger btn-trigger btn-icon" data-toggle="tooltip" data-placement="top" title="Hapus">
                                                <em class="icon ni ni-trash-alt"></em>
                                            </a>
                                        </li>
                                        <li>
                                            <div class="drodown">
                                                <a href="#" class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <ul class="link-list-opt no-bdr">
                                                        <li>
                                                            <a wire:click.prevent="terima({{$invoice->id}})" href="#">
                                                                <em class="icon ni ni-check-fill-c"></em>
                                                                <span>Terima</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="{{route('invoice.print', $invoice->id)}}">
                                                                <em class="icon ni ni-printer"></em>
                                                                <span>Print</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div><!-- .nk-tb-item -->
                        @endforeach
                    </div><!-- .nk-tb-list -->
                </div><!-- .card-inner -->

                <div class="card-inner">
                    <div class="nk-block-between-md g-3">
                        <div class="g">
                            <ul class="pagination justify-content-center justify-content-md-start">
                                {{$semuaInvoice->links('vendor.livewire.bootstrap')}}
                            </ul><!-- .pagination -->
                        </div>
                        <div class="g">
                            <div class="pagination-goto d-flex justify-content-center justify-content-md-start gx-3">
                                <div> Halaman {{$semuaInvoice->currentPage()}} Dari {{$semuaInvoice->lastPage()}}</div>
                            </div>
                        </div><!-- .pagination-goto -->
                    </div><!-- .nk-block-between -->
                </div><!-- .card-inner -->
            </div><!-- .card-inner-group -->
        </div><!-- .card -->
    </div><!-- .nk-block -->
    <div class="modal fade" tabindex="-1" id="modalForm">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Buat Invoice</h5>
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <em class="icon ni ni-cross"></em>
                    </a>
                </div>
                <div class="modal-body">
                    @if($statusUpdate)
                        @livewire('invoice.update')
                    @else
                        @livewire('invoice.create');
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>


@push('js')
    <script>
        Livewire.on('tambahInvoice', function (){
            $('#modalForm').modal('show');
            $('#form-pengguna').select2({
                placeholder: "Pilih Pengguna",
            });
        })

        Livewire.on('getInvoice', invoice => {
            console.log(invoice['pengguna_id'])
            $('#modalForm').modal('show');
            $('.modal-title').text('Sunting Invoice');
            $('#form-pengguna').val(invoice['pengguna_id']);
        })

        Livewire.on('closeModal', function (){
            $('#modalForm').modal('hide');
        })
    </script>
@endpush
