<div>
    {{-- The whole world belongs to you --}}
    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-head-sub">
            <a class="back-to" href="{{route('pengguna.index')}}">
                <em class="icon ni ni-arrow-left"></em>
                <span>Pengguna</span>
            </a>
        </div>
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">Daftar Pengguna</h3>
                <div class="nk-block-des text-soft">
                    <p>Silahkan Isi Data Sesuai Biodata Asli</p>
                </div>
            </div><!-- .nk-block-head-content -->
            <div class="nk-block-head-content">
                <div class="toggle-wrap nk-block-tools-toggle">
                    <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="pageMenu"><em class="icon ni ni-menu-alt-r"></em></a>
                    <div class="toggle-expand-content" data-content="pageMenu">
                        <ul class="nk-block-tools g-3">
                            <li><a href="#" class="btn btn-white btn-outline-light"><em class="icon ni ni-download-cloud"></em><span>Export</span></a></li>
                            <li class="nk-block-tools-opt">
                                <div class="drodown">
                                    <a href="{{route('pengguna.create')}}" class="dropdown-toggle btn btn-icon btn-primary"><em class="icon ni ni-plus"></em></a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div><!-- .toggle-wrap -->
            </div><!-- .nk-block-head-content -->
        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->

    <div class="nk-block">
        <div class="row">
            <div class="col-md-7">
                <div class="card text-secondary card-bordered">
                    <div class="card-inner">
                        <h5 class="card-title">Tambah Pengguna</h5>
                        <p class="card-subtitle">Formulir untuk menambah pengguna</p>
                        <form wire:submit.prevent="simpan" class="mt-3">
                            <div class="form-group">
                                <label class="form-label" for="nama-lengkap">Nama Lengkap</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-user"></em>
                                    </div>
                                    <input wire:model="nama_lengkap" type="text" class="form-control form-control-lg @error('nama_lengkap') error @enderror " id="nama-lengkap" placeholder="Masukan Nama Lengkap">
                                </div>
                                @error('nama_lengkap')
                                <span class="toast-error text-danger">{{$message}}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label class="form-label" for="nomor-meter">Nomor Meter</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-cards"></em>
                                    </div>
                                    <input wire:model="nomor_meter" type="text" class="form-control form-control-lg @error('nomor_meter') error @enderror" id="nomor-meter" placeholder="Masukan Nomor Meter">
                                </div>
                                @error('nomor_meter')
                                <span class="toast-error text-danger">{{$message}}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label class="form-label" for="telepon">Telepon</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-call"></em>
                                    </div>
                                    <input wire:model="nomor_telepon" type="text" class="form-control form-control-lg @error('nomor_telepon') error @enderror" id="telepon" placeholder="Masukan Nomor Telepon">
                                </div>
                                @error('nomor_telepon')
                                <span class="toast-error text-danger">{{$message}}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label class="form-label" for="telepon">Alamat Lengkap</label>
                                <textarea wire:model="alamat" class="form-control @error('alamat') error @enderror" id="" cols="30" rows="4"></textarea>
                                @error('alamat')
                                <span class="toast-error text-danger">{{$message}}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <button class="btn btn-primary float-right">
                                    <em class="icon ni ni-save mr-1"></em>
                                    Simpan Pengguna
                                </button>
                            </div>

                        </form>
                    </div>

                </div>
            </div>
            <div class="col-md-5">
                <div class="card card-bordered">
                    <div class="card-inner">
                        <div class="card-head">
                            <h5 class="card-title">Data Pengguna</h5>
                        </div>
                        <table class="table table-borderless">
                            <tr>
                                <th class="text-wrap">Nama Lengkap</th>
                                <td>:</td>
                                <td>{{$nama_lengkap}}</td>
                            </tr>
                            <tr>
                                <th class="text-wrap">Nomor Meter</th>
                                <td>:</td>
                                <td>{{$nomor_meter}}</td>
                            </tr>
                            <tr>
                                <th class="text-wrap">Telepon</th>
                                <td>:</td>
                                <td>{{$nomor_telepon}}</td>
                            </tr>
                            <tr>
                                <th class="text-wrap">Alamat Lengkap</th>
                                <td>:</td>
                                <td>{{$alamat}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
