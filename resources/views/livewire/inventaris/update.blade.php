<div>
    {{-- The best athlete wants his opponent at his best. --}}
    <form wire:submit.prevent="update">
        <div class="form-group">
            <label class="form-label" for="full-name">Nama Barang</label>
            <div class="form-control-wrap">
                <textarea wire:model="nama_barang" class="form-control" id="" cols="30" rows="3" placeholder="Contoh: Bakso Satu Gerobak"></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="form-label" for="full-name">Jumlah</label>
            <div class="form-control-wrap">
                <div class="form-icon form-icon-left">
                    <em class="icon ni ni-cart"></em>
                </div>
                <input type="number" wire:model="jumlah" class="form-control" id="akhir_meter" placeholder="1">
            </div>
        </div>
        <div class="form-group">
            <label class="form-label" for="full-name">Satuan</label>
            <div class="form-control-wrap">
                <div class="form-control-select">
                    <div class="form-icon form-icon-left">
                        <em class="icon ni ni-box"></em>
                    </div>
                    <select wire:model="satuan" class="form-control" id="default-06">
                        <option class="font-weight-bold">Pilih Satuan</option>
                        <option value="Pcs">Pcs</option>
                        <option value="Kg">Kg</option>
                    </select>
                </div>

            </div>
        </div>
        <div class="form-group">
            <label class="form-label" for="full-name">Harga Satuan</label>
            <div class="form-control-wrap"><div class="form-icon form-icon-left">
                    <em class="icon ni ni-sign-dollar"></em>
                </div>
                <input wire:model="harga_satuan" type="text" class="form-control" id="diskon" placeholder="1000">
            </div>
        </div>
        <div class="form-group">
            <label class="form-label" for="full-name">Tanggal Pengeluaran</label>
            <div class="form-control-wrap"><div class="form-icon form-icon-left">
                    <em class="icon ni ni-calender-date"></em>
                </div>
                <input wire:model="tanggal_pengeluaran" type="date" class="form-control" id="diskon" placeholder="1000">
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-dark">Update Pengeluaran</button>
        </div>
    </form>

</div>
