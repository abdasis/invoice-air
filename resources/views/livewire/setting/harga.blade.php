<div>
    {{-- Nothing in the world is as soft and yielding as water. --}}
    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between g-3">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">Pengaturan Harga</h3>
                <div class="nk-block-des text-soft">
                    <p>Pengaturan harga terbaru</p>
                </div>
            </div><!-- .nk-block-head-content -->
            <div class="nk-block-head-content">
                <ul class="nk-block-tools g-3">
                    <li>
                        <div class="drodown">
                            <a href="#" class="dropdown-toggle btn btn-icon btn-primary" data-toggle="dropdown"><em class="icon ni ni-plus"></em></a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <ul class="link-list-opt no-bdr">
                                    <li><a href="#"><span>Add User</span></a></li>
                                    <li><a href="#"><span>Add Team</span></a></li>
                                    <li><a href="#"><span>Import User</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                </ul>
            </div><!-- .nk-block-head-content -->
        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block">
        <div class="card card-bordered card-stretch">
            <div class="card-inner">
                <h5 class="card-title">Form Ubah Harga</h5>
                <p class="card-subtitle">Masukan Harga Sesuai yang ingin di tetapkan</p>
            </div>
            <div class="card-inner">
                <div class="row">
                    <div class="col-md-6">
                        <form wire:submit.prevent="simpan">
                            <div class="form-group">
                                <label class="form-label" for="nama-lengkap">Biaya Perawatan</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-money"></em>
                                    </div>
                                    <input wire:model="biaya_perawatan" type="text" class="form-control form-control-lg @error('biaya_perawatan') error @enderror " id="biaya_perawatan" placeholder="1000">
                                </div>
                                @error('biaya_perawatan')
                                <span class="toast-error text-danger">{{$message}}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label class="form-label" for="biaya_admin">Biaya Admin</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-money"></em>
                                    </div>
                                    <input wire:model="biaya_admin" type="text" class="form-control form-control-lg @error('biaya_admin') error @enderror " id="biaya_admin" placeholder="1000">
                                </div>
                                @error('biaya_admin')
                                <span class="toast-error text-danger">{{$message}}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label class="form-label" for="harga_perkubik">Harga Perkubik</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-money"></em>
                                    </div>
                                    <input wire:model="harga_perkubik" type="text" class="form-control form-control-lg @error('harga_perkubik') error @enderror " id="harga_perkubik" placeholder="1000">
                                </div>
                                @error('harga_perkubik')
                                <span class="toast-error text-danger">{{$message}}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <button class="btn btn-dark float-right"><em class="icon ni ni-save mr-1"></em> Simpan Perubahan</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <div class="card card-bordered card-full">
                            <div class="card-inner">
                                <div class="card-title-group align-start mb-0">
                                    <div class="card-title">
                                        <h6 class="subtitle">Harga Terkini</h6>
                                    </div>
                                    <div class="card-tools">
                                        <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Harga yang digunakan saat ini">

                                        </em>
                                    </div>
                                </div>
                                <div class="card-amount">
                                    <span class="amount text-success"> <span class="currency currency-usd">IDR</span> {{rupiah($semuaHarga->first()->harga_perkubik ?? 0)}} </span>
                                </div>
                                <div class="invest-data">
                                    <div class="invest-data-amount g-2"><div class="invest-data-history"><div class="title">Biaya Admin</div><div class="amount"><span class="currency currency-usd">USD</span> {{rupiah($semuaHarga->first()->biaya_admin ?? 0)}}
                                            </div>
                                        </div>
                                        <div class="invest-data-history">
                                            <div class="title">Biaya Perawatan</div>
                                            <div class="amount"> <span class="currency currency-usd">IDR</span>
                                                {{rupiah($semuaHarga->first()->biaya_perawatan ?? 0)}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="invest-data-ck">
                                        <div class="chartjs-size-monitor">
                                            <div class="chartjs-size-monitor-expand">
                                                <div class="">

                                                </div>
                                            </div>
                                            <div class="chartjs-size-monitor-shrink">
                                                <div class="">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card card-bordered card-stretch">
            <div class="card-inner-group">
                <div class="card-inner">
                    <div class="card-title-group">
                        <div class="card-title">
                            <h5 class="title">Data Perubahan Harga</h5>
                        </div>
                        <div class="card-tools mr-n1">
                            <ul class="btn-toolbar">
                                <li>
                                    <a href="#" class="btn btn-icon search-toggle toggle-search" data-target="search"><em class="icon ni ni-search"></em></a>
                                </li><!-- li -->
                                <li class="btn-toolbar-sep"></li><!-- li -->
                                <li>
                                    <div class="dropdown">
                                        <a href="#" class="btn btn-trigger btn-icon dropdown-toggle" data-toggle="dropdown">
                                            <em class="icon ni ni-setting"></em>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-xs dropdown-menu-right">
                                            <ul class="link-check">
                                                <li><span>Show</span></li>
                                                <li class="{{$paginate == 10 ? 'active' : ''}}"><a wire:click="sepuluh">10</a></li>
                                                <li class="{{$paginate == 20 ? 'active' : ''}}"><a wire:click="duaPuluh">20</a></li>
                                                <li class="{{$paginate == 50 ? 'active' : ''}}"><a wire:click="limaPuluh">50</a></li>
                                            </ul>
                                            <ul class="link-check">
                                                <li><span>Order</span></li>
                                                <li class="{{$sort == 'desc' ? 'active' : ''}}"><a wire:click="desc">DESC</a></li>
                                                <li class="{{$sort == 'asc' ? 'active' : ''}}"><a wire:click="asc">ASC</a></li>
                                            </ul>
                                        </div>
                                    </div><!-- .dropdown -->
                                </li><!-- li -->
                            </ul><!-- .btn-toolbar -->
                        </div><!-- card-tools -->
                        <div class="card-search search-wrap" data-search="search">
                            <div class="search-content">
                                <a href="#" class="search-back btn btn-icon toggle-search" data-target="search"><em class="icon ni ni-arrow-left"></em></a>
                                <input type="text" class="form-control form-control-sm border-transparent form-focus-none" placeholder="Quick search by order id">
                                <button class="search-submit btn btn-icon"><em class="icon ni ni-search"></em></button>
                            </div>
                        </div><!-- card-search -->
                    </div><!-- .card-title-group -->
                </div><!-- .card-inner -->
                <div class="card-inner p-0">
                    <table class="table table-tranx">
                        <thead>
                        <tr class="tb-tnx-head">
                            <th class="tb-tnx-id"><span class="">#</span></th>
                            <th class="tb-tnx-info">
                            <span class="tb-tnx-desc d-none d-sm-inline-block">
                                <span>Perubahan</span>
                            </span>
                                <span class="tb-tnx-date d-md-inline-block d-none">
                                <span class="d-md-none">Harga</span>
                                <span class="d-none d-md-block">
                                    <span>Biaya Perawatan</span>
                                    <span>Biaya Admin</span>
                                </span>
                            </span>
                            </th>
                            <th class="tb-tnx-amount is-alt">
                                <span class="tb-tnx-total">Biaya Perkubik</span>
                                <span class="tb-tnx-status d-none d-md-inline-block">Dibuat Oleh</span>
                            </th>
                            <th class="tb-tnx-action">
                                <span>&nbsp;</span>
                            </th>
                        </tr><!-- tb-tnx-item -->
                        </thead>
                        <tbody>
                        @foreach($semuaHarga as $key => $harga)
                            <tr class="tb-tnx-item">
                                <td class="tb-tnx-id">
                                    <a href="#"><span>{{$key+1}}</span></a>
                                </td>
                                <td class="tb-tnx-info">
                                    <div class="tb-tnx-desc">
                                        <span class="title">{{\Carbon\Carbon::parse($harga->created_at)->format('d M Y')}}</span>
                                    </div>
                                    <div class="tb-tnx-date">
                                        <span class="date">Rp. {{rupiah($harga->biaya_perawatan)}}</span>
                                        <span class="date">Rp. {{rupiah($harga->biaya_admin)}}</span>
                                    </div>
                                </td>
                                <td class="tb-tnx-amount is-alt">
                                    <div class="tb-tnx-total">
                                        <span class="amount">Rp. {{rupiah($harga->harga_perkubik)}}</span>
                                    </div>
                                    <div class="tb-tnx-status">
                                        <span class="badge badge-dot badge-success">{{$harga->dibuat_oleh}}</span>
                                    </div>
                                </td>
                                <td class="tb-tnx-action">
                                    <div class="dropdown">
                                        <a class="text-soft dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-xs">
                                            <ul class="link-list-plain">
                                                <li><a wire:click.prevent="delete({{$harga->id}})" class="text-danger" href="#">Remove</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr><!-- tb-tnx-item -->
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- .card-inner -->
                <div class="card-inner">
                    <ul class="pagination justify-content-center justify-content-md-start">
                        {{$semuaHarga->links()}}
                    </ul><!-- .pagination -->
                </div><!-- .card-inner -->
            </div><!-- .card-inner-group -->
        </div><!-- .card -->
    </div><!-- .nk-block -->
</div>
