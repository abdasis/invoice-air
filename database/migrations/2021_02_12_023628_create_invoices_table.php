<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->integer('awal_meter');
            $table->integer('akhir_meter');
            $table->integer('pemakaian_liter');
            $table->integer('pemakain_kubik');
            $table->integer('biaya_admin');
            $table->integer('biaya_perawatan');
            $table->integer('harga_perkubik');
            $table->integer('diskon')->nullable();
            $table->integer('total_tagihan');
            $table->enum('status', ['lunas', 'belum bayar'])->default('belum bayar');
            $table->foreignId('pengguna_id')->constrained()->cascadeOnDelete();
            $table->string('dibuat_oleh');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
